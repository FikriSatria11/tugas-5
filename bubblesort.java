import java.io.IOException;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Scanner;

public class bubblesort{
	static Scanner input=new Scanner(System.in);
	
	static Object[][] arr=new Object[11][5];
	
	static int count=0;
	
	static String choice;
	static String menuChoice;
	static int kode;
	static String nama;
	static String warna;
	static int jumlah;
	static int harga;
	
	  public static void main(String[]args) throws IOException{
		  PrintWriter out = new PrintWriter("Inventory.txt");
			PrintWriter sort = new PrintWriter("SortedInventory.txt");
		try{
			
		
		
		boolean terminate;
		do{
			System.out.println("1. Tambahkan Barang");
			System.out.println("2. Tampilkan Barang");
			
			System.out.print(">> ");
			menuChoice = input.nextLine();
			
			if(menuChoice.contentEquals("0")){
				break;
			}
			
			switch(menuChoice){
			case "1":
				
				 do{
					 
					 terminate=true;
					 
					 do{
						 
							 System.out.print("Code : ");
						 try{
							 kode=input.nextInt();
							 break;
							 
						 }catch(InputMismatchException e){
							
							 System.out.println("Code harus berupa nomor");
							 System.out.println("ulangi");
							 input.nextLine();
							 continue;
							 
						 }
					 }while(true);
					  
					 for(int index=0;index<count;index++){
						 if(kode==(int)arr[index][0]){
							 terminate=false;
							 System.out.println("Code sudah digunakan");
							 System.out.println("ulangi\n");
						 }
						 
					 }
					 
				 }while(terminate==false);
				 input.nextLine();
				 do{
					 terminate=true;
					 System.out.print("Nama : ");
					 nama=input.nextLine();
					 for(int index=0;index<count;index++){
						 if(nama.contentEquals((String) arr[index][1])){
							 terminate=false;
							 System.out.println("Nama sudah digunakan");
							 System.out.println("ulangi\n");
						 }
						 
					 }
					 if(nama.matches(".*\\d+.*")){
						 terminate=false;
						 System.out.println("Nama harus berupa huruf");
						 System.out.println("ulangi\n");
					 }
					 
				 }while(terminate==false);
				 
				 do{
					 terminate=true;
					 System.out.print("warna : ");
					 warna=input.nextLine();
					 if(warna.matches(".*\\d+.*")){
						 terminate=false;
						 System.out.println("warna harus berupa huruf");
						 System.out.println("ulangi\n");
					 }
					 
					 
				 }while(terminate==false);
				
				 
					 
					 do{
						 
							 System.out.print("Total stok : ");
						 try{
							 jumlah=input.nextInt();
							 break;
							 
						 }catch(InputMismatchException e){
							
							 System.out.println("Total Stok harus berupa angka");
							 System.out.println("ulangi");
							 input.nextLine();
							 continue;
							 
						 }
					 }while(true);
					  
					 
				 do{
						 
						 System.out.print("harga : ");
					 try{
						 harga=input.nextInt();
						 break;
						 
					 }catch(InputMismatchException e){
						
						 System.out.println("harga harus berupa angka");
						 System.out.println("ulangi");
						 input.nextLine();
						 continue;
						 
					 }
				 }while(true);
				 
				
				 
				 
				 
				 
				    arr[count][0] = kode;
				    arr[count][1] = nama;
				    arr[count][2] = warna;
				    arr[count][3] = jumlah;
				    arr[count][4] = harga;
				    
				    out.println(kode);
					out.println(nama);
					out.println(warna);
					out.println(jumlah);
					out.println(harga);
					out.println("====");
					
					
				    
				   count++;
				input.nextLine();
				break;
				
				
				
			case "2":
				System.out.println("Sort by: ");
				System.out.println("1. Kode");
				System.out.println("2. Nama");
				System.out.println("3. Warna");
				System.out.println("4. Jumlah");
				System.out.println("5. Harga");
				System.out.print(">> ");
				choice=input.nextLine();
				
				
				switch(choice){
				case "1":
					
					String temp="";
					int tempInt=-1;
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<count;secondIndex++){
							if( ( (int) arr[index][0] ) < ((int)arr[secondIndex][0])){
								
								 tempInt = (int) arr[secondIndex][0];  
                                 arr[secondIndex][0] = arr[index][0];  
                                 arr[index][0] = tempInt;
                                 
                                 temp = (String) arr[secondIndex][1];  
                                 arr[secondIndex][1] = arr[index][1];  
                                 arr[index][1] = temp;
                                 
                                 temp = (String) arr[secondIndex][2];  
                                 arr[secondIndex][2] = arr[index][2];  
                                 arr[index][2] = temp;
                                 
                                 tempInt = (int) arr[secondIndex][3];  
                                 arr[secondIndex][3] = arr[index][3];  
                                 arr[index][3] = tempInt;
                                 
                                 tempInt = (int) arr[secondIndex][4];  
                                 arr[secondIndex][4] = arr[index][4];  
                                 arr[index][4] = tempInt;
							}
						}
					}
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<5;secondIndex++){
							sort.println(arr[index][secondIndex]);
						}
						sort.println("====");
					}
					sort.close();
					System.out.println("SortedInventory.txt sukses dibuat");
					System.out.println("file .txt dapat ditemukan di folder yang sama dengan file .java ini");
					break;
				case "2":
					temp="";
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<count;secondIndex++){
							if(((String) arr[index][1]).compareToIgnoreCase((String) arr[secondIndex][1]) < 0){
								
								tempInt = (int) arr[secondIndex][0];  
                                arr[secondIndex][0] = arr[index][0];  
                                arr[index][0] = tempInt;
                                
                                temp = (String) arr[secondIndex][1];  
                                arr[secondIndex][1] = arr[index][1];  
                                arr[index][1] = temp;
                                
                                temp = (String) arr[secondIndex][2];  
                                arr[secondIndex][2] = arr[index][2];  
                                arr[index][2] = temp;
                                
                                tempInt = (int) arr[secondIndex][3];  
                                arr[secondIndex][3] = arr[index][3];  
                                arr[index][3] = tempInt;
                                
                                tempInt = (int) arr[secondIndex][4];  
                                arr[secondIndex][4] = arr[index][4];  
                                arr[index][4] = tempInt;
							}
						}
					}
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<5;secondIndex++){
							sort.println(arr[index][secondIndex]);
						}
						sort.println("====");
					}
					sort.close();
					System.out.println("SortedInventory.txt sukses dibuat");
					System.out.println("file .txt dapat ditemukan di folder yang sama dengan file .java ini");
					break;
				case "3":
					temp="";
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<count;secondIndex++){
							if(((String) arr[index][2]).compareToIgnoreCase((String) arr[secondIndex][2]) < 0){
								
								tempInt = (int) arr[secondIndex][0];  
                                arr[secondIndex][0] = arr[index][0];  
                                arr[index][0] = tempInt;
                                
                                temp = (String) arr[secondIndex][1];  
                                arr[secondIndex][1] = arr[index][1];  
                                arr[index][1] = temp;
                                
                                temp = (String) arr[secondIndex][2];  
                                arr[secondIndex][2] = arr[index][2];  
                                arr[index][2] = temp;
                                
                                tempInt = (int) arr[secondIndex][3];  
                                arr[secondIndex][3] = arr[index][3];  
                                arr[index][3] = tempInt;
                                
                                tempInt = (int) arr[secondIndex][4];  
                                arr[secondIndex][4] = arr[index][4];  
                                arr[index][4] = tempInt;
							}
						}
					}
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<5;secondIndex++){
							sort.println(arr[index][secondIndex]);
						}
						sort.println("====");
					}
					sort.close();
					System.out.println("SortedInventory.txt sukses dibuat");
					System.out.println("file .txt dapat ditemukan di folder yang sama dengan file .java ini");
					break;
				case "4":
					temp="";
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<count;secondIndex++){
							if( ( (int) arr[index][3] ) < ((int)arr[secondIndex][3])){
								
								tempInt = (int) arr[secondIndex][0];  
                                arr[secondIndex][0] = arr[index][0];  
                                arr[index][0] = tempInt;
                                
                                temp = (String) arr[secondIndex][1];  
                                arr[secondIndex][1] = arr[index][1];  
                                arr[index][1] = temp;
                                
                                temp = (String) arr[secondIndex][2];  
                                arr[secondIndex][2] = arr[index][2];  
                                arr[index][2] = temp;
                                
                                tempInt = (int) arr[secondIndex][3];  
                                arr[secondIndex][3] = arr[index][3];  
                                arr[index][3] = tempInt;
                                
                                tempInt = (int) arr[secondIndex][4];  
                                arr[secondIndex][4] = arr[index][4];  
                                arr[index][4] = tempInt;
							}
						}
						
					}
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<5;secondIndex++){
							sort.println(arr[index][secondIndex]);
						}sort.println("====");
					}
					sort.close();
					System.out.println("SortedInventory.txt sukses dibuat");
					System.out.println("file .txt dapat ditemukan di folder yang sama dengan file .java ini");
					break;
				case "5":
					temp="";
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<count;secondIndex++){
							if( ( (int) arr[index][4] ) < ((int)arr[secondIndex][4])){
								
								tempInt = (int) arr[secondIndex][0];  
                                arr[secondIndex][0] = arr[index][0];  
                                arr[index][0] = tempInt;
                                
                                temp = (String) arr[secondIndex][1];  
                                arr[secondIndex][1] = arr[index][1];  
                                arr[index][1] = temp;
                                
                                temp = (String) arr[secondIndex][2];  
                                arr[secondIndex][2] = arr[index][2];  
                                arr[index][2] = temp;
                                
                                tempInt = (int) arr[secondIndex][3];  
                                arr[secondIndex][3] = arr[index][3];  
                                arr[index][3] = tempInt;
                                
                                tempInt = (int) arr[secondIndex][4];  
                                arr[secondIndex][4] = arr[index][4];  
                                arr[index][4] = tempInt;
							}
						}
						
					}
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<5;secondIndex++){
							sort.println(arr[index][secondIndex]);
						}sort.println("====");
					}
					sort.close();
					System.out.println("SortedInventory.txt sukses dibuat");
					System.out.println("file .txt dapat ditemukan di folder yang sama dengan file .java ini");
					break;
				default:
					
					break;
						
				}
				
				out.close();
				System.out.println("Inventory.txt sukses dibuat");
				System.out.println("file .txt dapat ditemukan di folder yang sama dengan file .java ini");
				break;
			
			default:
				System.out.println("silahkan input angka 1 atau 2 ");
				System.out.println("ulangi\n");
				break;
			}
					
			
		}while(menuChoice.contentEquals("2")==false);		
	}finally{
		out.close();
		sort.close();
	}
	  }
	 
}
